<!DOCTYPE html>
<html>
<head>
	<base href="<?php $url_info = parse_url( home_url() ); echo trailingslashit( $url_info['path'] ); ?>">
	<title>Home | AngularJS Demo Theme</title>
	<?php wp_head(); ?>
</head>
<body>
    <div class="body_header">
    <h4>Moxie movies list:</h4>
    </div>
	<div id="page" ng-app="app">
		<header class="text-center">
			<h1>
				<a href="<?php echo home_url(); ?>"></a>
			</h1>
		</header>
		
		<div class="container">
			<div ng-view></div>			
		</div>

		<footer>
			&copy; <?php echo date( 'Y' ); ?>
		</footer>
	</div>

	<?php wp_footer(); ?>
</body>
</html>