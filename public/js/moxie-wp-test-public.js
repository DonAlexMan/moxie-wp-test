var app = angular.module('app', ['ngRoute', 'ngAnimate']);

app.config( function ($routeProvider, $locationProvider) {

	$locationProvider.html5Mode(true);

	$routeProvider.when('/', {
		templateUrl: myLocalized.partials + 'main.html',
		controller: 'MainCtrl'
	});
	$routeProvider.when('/:ID', {
		templateUrl: myLocalized.partials + 'content.html',
		controller: 'ContentCtrl'
	});
});

app.controller('MainCtrl', function ( $scope, $http, $routeParams) {
	$http.get('wp-admin/admin-ajax.php?action=movie').success( function(res){
		if( angular.isArray(res) &&  (res.length > 0) ) {
		    //handler either not an array or empty array
		    $scope.movies = res;
		}else{
			$scope.noMovies = true;
		}
		
	});
});

app.controller('ContentCtrl', function ( $scope, $http, $routeParams ) {
	$http.get( 'wp-json/movies/' + $routeParams.ID ).success( function(res){
		$scope.movie = res;
	});
});