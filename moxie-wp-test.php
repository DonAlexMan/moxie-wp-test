<?php
/*
 * Plugin Name:       Moxie WP Test
 * Plugin URI:        http://github.com/gpnalin/moxie-wp-test
 * Description:       Movies Library Displays the posts on homepage using REST API.
 * Version:           0.2.0
 * Author:            Alex Manrique
 * Author URI:        http://www.alexmanrique.com
 * Text Domain:       moxie-wp-test-locale
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Domain Path:       /languages
 */
 
 // If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
    die;
}
 
/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-moxie-wp-test-activator.php
 */
function activate_moxie_wp_test() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-moxie-wp-test-activator.php';
	Moxie_WP_Test_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-moxie-wp-test-deactivator.php
 */
function deactivate_moxie_wp_test() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-moxie-wp-test-deactivator.php';
	Moxie_WP_Test_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_moxie_wp_test' );
register_deactivation_hook( __FILE__, 'deactivate_moxie_wp_test' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require_once plugin_dir_path( __FILE__ ) . 'includes/class-moxie-wp-test.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */	
function run_moxie_wp_test() {
 
    $mwpt = new Moxie_WP_Test();
    $mwpt->run();
 
} 
run_moxie_wp_test();