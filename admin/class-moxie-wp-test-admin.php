<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and hooks for
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/admin
 * @author     Nalin Perera <gpnalin@gmail.com>
 */
class Moxie_WP_Test_Admin {
    
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string    $version    The current version of this plugin.
     */
    private $version;
    
    /**
     * Initialize the class and set its properties. And hook ACF plugin with this plugin.
     *
     * @since    1.0.0
     * @param      string    $version    The version of this plugin.
     */
    public function __construct( $version ) {

        $this->version = $version;

		$this->configureACF();

    }
    
    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles() {
        
        /**
         * Enqueue admin stylesheets
         *
         * An instance of this class should be passed to the run() function
         * defined in Moxie_Wp_Test_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Moxie_Wp_Test_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_style(
            'single-post-meta-manager-admin',
            plugin_dir_url( __FILE__ ) . 'css/moxie-wp-test-admin.css',
            array(),
            $this->version,
            FALSE
        );
 
    }

    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts() {
        /**
         * Enqueue admin scripts
         *
         * An instance of this class should be passed to the run() function
         * defined in Moxie_Wp_Test_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The Moxie_Wp_Test_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script( 
            'single-post-meta-manager-admin', 
            plugin_dir_url( __FILE__ ) . 'js/moxie-wp-test-admin.js', 
            array( 'jquery' ),
            $this->version, FALSE
        );
    }

    /**
     * Extending ACF plugin to use in this plugin.
     *
     * @since    1.0.0
     */
	private function configureACF()
    {
        define( 'ACF_LITE' , true );
        include_once( plugin_dir_path( __FILE__ ) . 'acf/acf.php' );        
		include_once( plugin_dir_path( __FILE__ ) . 'acf/register-custom-fields.php' );
	}

    /**
     * Registers custom api endpoint for movie custom post type.
     *
     * @since    1.0.0
     * @param      string    $server    The post data.
     */
    public function api_init_movie_handle( $server ) {
        global $moxie_wp_test_api_movie;
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'includes/class-moxie-wp-test-api-movie.php';
        $moxie_wp_test_api_movie = new Moxie_WP_Test_API_Movie( $server );
        $moxie_wp_test_api_movie->register_filters();
    }

    /**
     * Filter api data feeds.
     *
     * This function filter what we sent through json api.
     *
     * @since    1.0.0
     * @param      object    $data    The data.
     * @param      object    $post    The post data.
     * @param      object    $context    The api context.
     */
    public function remove_extra_data( $data, $post, $context ) {

        // We only want to modify the 'view' context, for reading posts
        if ( $context !== 'view' || is_wp_error( $data ) ) {
         return $data;
        }

        // assigns $data to our variable
        $_data = $data;

        // checks for post having a thumbnail.
        if ( has_post_thumbnail($data['ID']) ) {

            // get thubnail id using post id
            $thumbnail_id = get_post_thumbnail_id( $data['ID'] );

            // full image
            $poster_url = wp_get_attachment_image_src( $thumbnail_id, 'full' );
            $_data['poster_url'] = $poster_url[0];

            // thumbnail image
            $poster_url_thumb = wp_get_attachment_image_src( $thumbnail_id, 'medium' );            
            $_data['poster_url_thumb'] = $poster_url_thumb[0];

        }

        // check if post has any acf data
        if ( $fields = get_fields($data['ID'])) {

            // go through all acf fields
            foreach( $fields as $key => $value )
            {
                // - $value has already been loaded for us, no point to load it again in the get_field_object function
                $field = get_field_object($key, false, array('load_value' => false));

                // check for release date, because we want to send only release year.
                if ( !($key=='release_date') ) {

                    // set key value pair to send through api.
                    $_data[$key] = $value;

                }else{

                    // we don't need release dat so we get the year from movie release date.
                    $date = DateTime::createFromFormat('Ymd', $value );
                    $_data['year'] = $date->format('Y');  

                }
                
            }
        }

        // filter all html tags from the content and send just plain texts
        if ( $data['content'] ) {

            $content = wp_strip_all_tags( $data['content'] );

            $_data['short_description'] = preg_replace('/((\w+\W*){16}(\w+))(.*)/', '${1}', $content) . '...';

            $_data['description'] = $content;

        }

        // Here, we unset any data we don't want to see on the front end:
        unset( $_data['type'] );
        unset( $_data['author'] );
        unset( $_data['parent'] );
        unset( $_data['date'] );
        unset( $_data['modified'] );
        unset( $_data['format'] );
        unset( $_data['guid'] );
        unset( $_data['excerpt'] );
        unset( $_data['menu_order'] );
        unset( $_data['comment_status'] );
        unset( $_data['ping_status'] );
        unset( $_data['sticky'] );
        unset( $_data['date_tz'] );
        unset( $_data['date_gmt'] );
        unset( $_data['modified_tz'] );
        unset( $_data['modified_gmt'] );
        unset( $_data['terms'] );
        unset( $_data['featured_image']  );
        unset( $_data['link'] );
        unset( $_data['status'] );
        unset( $_data['content'] );

        $data = $_data;

        return $data;

    }

    /**
     * Register the required plugins for this theme.
     *
     * In this example, we register five plugins:
     * - one included with the TGMPA library
     * - two from an external source, one from an arbitrary source, one from a GitHub repository
     * - two from the .org repo, where one demonstrates the use of the `is_callable` argument
     *
     * The variable passed to tgmpa_register_plugins() should be an array of plugin
     * arrays.
     *
     * This function is hooked into tgmpa_init, which is fired within the
     * TGM_Plugin_Activation class constructor.
     */
    function my_theme_register_required_plugins() {

        /*
         * Array of plugin arrays. Required keys are name and slug.
         * If the source is NOT from the .org repo, then source is also required.
         */
        $plugins = array(
            // This is an example of how to include a plugin from the WordPress Plugin Repository.
            array(
                'name'      => 'WP REST API (WP API)',
                'slug'      => 'json-rest-api',
                'required'  => true,
            )
        );

        /*
         * Array of configuration settings. Amend each line as needed.
         *
         * TGMPA will start providing localized text strings soon. If you already have translations of our standard
         * strings available, please help us make TGMPA even better by giving us access to these translations or by
         * sending in a pull-request with .po file(s) with the translations.
         *
         * Only uncomment the strings in the config array if you want to customize the strings.
         */
        $config = array(
            'id'           => 'moxie',                  // Unique ID for hashing notices for multiple instances of TGMPA.
            'default_path' => '',                       // Default absolute path to bundled plugins.
            'menu'         => 'tgmpa-install-plugins',  // Menu slug.
            'parent_slug'  => 'plugins.php',            // Parent menu slug.
            'capability'   => 'install_plugins',        // Capability needed to view plugin install page, should be a capability associated with the parent menu used.
            'has_notices'  => true,                     // Show admin notices or not.
            'dismissable'  => false,                    // If false, a user cannot dismiss the nag message.
            'dismiss_msg'  => '',                       // If 'dismissable' is false, this message will be output at top of nag.
            'is_automatic' => false,                    // Automatically activate plugins after installation or not.
            'message'      => '',                       // Message to output right before the plugins table.
            
            'strings'      => array(
                'notice_can_install_required'     => _n_noop(
                    'This Moxie WP Test requires the following plugin: %1$s.',
                    'This Moxie WP Test requires the following plugins: %1$s.'
                ), // %1$s = plugin name(s).
                'notice_can_install_recommended'  => _n_noop(
                    'This Moxie WP Test recommends the following plugin: %1$s.',
                    'This Moxie WP Test recommends the following plugins: %1$s.'
                )
            ),
        );
        tgmpa( $plugins, $config );

    }


    function get_movies(){

        $data = (array) get_transient( 'movies' );
        // If not in cache, get from database
        if ( !isset( $data ) ) {
            $movies = get_posts( array(
                'post_type' => 'movie',
                //'meta_key' => 'year',
                'orderby' => 'meta_value_num',
                'order' => 'desc',
                'posts_per_page' => 30
            ) );
            // Format as required for JSON output
            $rows = array();
            foreach ( $movies as $movie ) {
                $rows[] = array(
                    'id' => $movie->ID,
                    'title' => $movie->post_title,
                    'poster_url' => get_post_meta( $movie->ID, 'poster_url', true ),
                    'rating' => intval( get_post_meta( $movie->ID, 'rating', true ) ),
                    'year' => get_post_meta( $movie->ID, 'year', true ),
                    'short_description' => get_post_meta( $movie->ID, 'description', true )
                );
            }
            // Keep in the cache for an hour
            $data = $rows;
            set_transient( 'movies', $data );
        }
        // Output to the browser
        wp_send_json( $data );
        wp_die();
    }
    /**
     * Clears cache when data changes
     */
    function clear_cache() {
        delete_transient( 'movies' );
    }
 
}