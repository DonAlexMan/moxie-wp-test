<?php
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_cpt-movie',
		'title' => 'Movie Info',
		'fields' => array (
			array (
				'key' => 'field_5631ed0d365e5',
				'label' => 'Release Date',
				'name' => 'year',
				'type' => 'date_picker',
				'instructions' => 'Movie release date.',
				'required' => 1,
				'date_format' => 'yy',
				'display_format' => 'yy',
				'first_day' => 1,
			),
			array (
				'key' => 'field_5631ed4f365e6',
				'label' => 'Genere',
				'name' => 'moxie_genere',
				'type' => 'checkbox',
				'choices' => array (
					'Thriller' => 'Thriller',
					'Horror' => 'Horror',
					'Sci-Fi' => 'Sci-Fi',
					'Action' => 'Action',
					'Erotic' => 'Erotic',
					'Comedy' => 'Comedy',
					'Romantic' => 'Romantic',
				),
				'default_value' => '',
				'layout' => 'horizontal',
			),
			array (
				'key' => 'field_56336e01ea508',
				'label' => 'Rating',
				'name' => 'rating',
				'type' => 'radio',
				'required' => 1,
				'choices' => array (
					1 => 1,
					2 => 2,
					3 => 3,
					4 => 4,
					5 => 5,
					6 => 6,
					7 => 7,
					8 => 8,
					9 => 9,
					10 => 10,
				),
				'other_choice' => 0,
				'save_other_choice' => 0,
				'default_value' => '',
				'layout' => 'horizontal',
			),
            array (
                'key' => 'field_5633443ea508',
                'label' => 'Poster url',
                'name' => 'poster_url',
                'type' => 'text',
                'required' => 0,
                'default_value' => '',
                'layout' => 'horizontal',
            ),
            array (
                'key' => 'field_5603443ea508',
                'label' => 'Poster url thumb',
                'name' => 'poster_url_thumb',
                'type' => 'text',
                'required' => 0,
                'default_value' => '',
                'layout' => 'horizontal',
            ),
            array (
                'key' => 'field_163143ea508',
                'label' => 'Short description',
                'name' => 'description',
                'type' => 'text',
                'required' => 1,
                'default_value' => '',
                'layout' => 'horizontal',
            )
		),
		'location' => array (
			array (
				array (
					'param' => 'post_type',
					'operator' => '==',
					'value' => 'movie',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'default',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}
