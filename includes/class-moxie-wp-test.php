<?php
 
 /**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       http://nalin.xyz
 * @since      1.0.0
 *
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/includes
 */

/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique current version of the plugin.
 *
 * @since      1.0.0
 * @package    Moxie_Wp_Test
 * @subpackage Moxie_Wp_Test/includes
 * @author     Nalin Perera <gpnalin@gmail.com>
 */
class Moxie_WP_Test {
    
    /**
     * The loader that's responsible for maintaining and registering all hooks that power
     * the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      Moxie_Wp_Test_Loader    $loader    Maintains and registers all hooks for the plugin.
     */
    protected $loader;
    
    /**
     * The unique identifier of this plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $plugin_slug    The string used to uniquely identify this plugin.
     */
    protected $plugin_slug;
    
    /**
     * The current version of the plugin.
     *
     * @since    1.0.0
     * @access   protected
     * @var      string    $version    The current version of the plugin.
     */
    protected $version;
    
    /**
     * Define the core functionality of the plugin.
     *
     * Set the plugin name and the plugin version that can be used throughout the plugin.
     * Load the dependencies, define the locale, and set the hooks for the admin area and
     * the public-facing side of the site.
     *
     * @since    1.0.0
     */
    public function __construct() {
 
        $this->plugin_slug = 'moxie-wp-test';
        $this->version = '1.0.0';
 
        $this->load_dependencies();
		$this->register_post_types();
        $this->define_admin_hooks();
        $this->define_public_hooks();   
    }
    
    /**
     * Load the required dependencies for this plugin.
     *
     * Include the following files that make up the plugin:
     *
     * - Moxie_Wp_Test_Loader. Orchestrates the hooks of the plugin.
     * - Moxie_Wp_Test_i18n. Defines internationalization functionality.
     * - Moxie_Wp_Test_Admin. Defines all hooks for the admin area.
     * - Moxie_Wp_Test_Public. Defines all hooks for the public side of the site.
     *
     * Create an instance of the loader which will be used to register the hooks
     * with WordPress.
     *
     * @since    1.0.0
     * @access   private
     */
    private function load_dependencies() {
        
        /**
         * The class responsible for defining all actions that occur in the admin area.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'admin/class-moxie-wp-test-admin.php';

        /**
         * The class responsible for defining all actions that occur in the public-facing
         * side of the site.
         */
        require_once plugin_dir_path( dirname( __FILE__ ) ) . 'public/class-moxie-wp-test-public.php';

        /**
         * The class responsible for defining cutom post types and assigning meta boxes/ taxanomies
         * if taxanomies or meta boxes isn't created this class create and assign them to the post type.
         */
        require_once plugin_dir_path( __FILE__ ) . 'class-custom-post-type.php';

        /**
         * The class responsible for checking and installing 3rd party plugins.
         */
        require_once plugin_dir_path( __FILE__ ) . 'class-tgm-plugin-activation.php';
        
        /**
         * The class responsible for orchestrating the actions and filters of the
         * core plugin.
         */
        require_once plugin_dir_path( __FILE__ ) . 'class-moxie-wp-test-loader.php';
        
        $this->loader = new Moxie_WP_Test_Loader();
 
    }
    
    /**
     * Register all of the hooks related to the admin area functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_admin_hooks() {
 
        $plugin_admin = new Moxie_WP_Test_Admin( $this->get_version() );

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_styles' );

        $this->loader->add_action( 'admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts' );

        $this->loader->add_action( 'tgmpa_register', $plugin_admin, 'my_theme_register_required_plugins' );        
            
        $this->loader->add_action( 'wp_json_server_before_serve', $plugin_admin, 'api_init_movie_handle' );

        $this->loader->add_filter( 'json_prepare_movie', $plugin_admin, 'remove_extra_data', 10, 3    );

        $this->loader->add_action( 'wp_ajax_movie', $plugin_admin, 'get_movies');

        $this->loader->add_action( 'wp_ajax_nopriv_movie', $plugin_admin, 'get_movies');

        $this->loader->add_action( 'after_delete_post', $plugin_admin, 'clear_cache' );

        $this->loader->add_action( 'save_post_movie', $plugin_admin, 'clear_cache' );

	}

    /**
     * Register all of the hooks related to the public-facing functionality
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
    private function define_public_hooks() {

        $plugin_public = new Moxie_Wp_Test_Public( $this->get_version() );

        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
        $this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
        $this->loader->add_filter( 'template_include', $plugin_public, 'replace_home_page' );

    }

    /**
     * Registers custom post types
     * of the plugin.
     *
     * @since    1.0.0
     * @access   private
     */
	private function register_post_types()
    {
        $movie = new Custom_Post_Type( 'Movie' );        
    }
    
    /**
     * Run the loader to execute all of the hooks with WordPress.
     *
     * @since    1.0.0
     */
    public function run() {
        $this->loader->run();
    }
    
    /**
     * Retrieve the version number of the plugin.
     *
     * @since     1.0.0
     * @return    string    The version number of the plugin.
     */
    public function get_version() {
        return $this->version;
    }
 
}